using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Examen_Ing_de_software
{
    public partial class Prueba : Form
    {
        int resultado = 0;
        int seleccion = 1;
        public Prueba()
        {
            InitializeComponent();
        }

        private void Form1_Load_1(object sender, EventArgs e)
        {
            Iniciar();
        }

        private void Iniciar()
        {
            if (MessageBox.Show("Bienvenido a la evaluación sobre Virtualizacion", "Evaluacion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
            {
                this.pregunta.Text = "Que es la virtualizacion de servidores ?";
                this.opcion1.Text = "Es un entorno que proporciona un enfoque para el diseño, planificación, implementación de maquinas vituales";
                this.opcion2.Text = "abstrae el software de servidor del hardware por invitado/host, lo que facilita que varios servidores virtuales se ejecuten en un dispositivo físico";
                this.opcion3.Text = "Es un framework de Arquitectura que proporciona un enfoque para el diseño, planificación, implementación y gobierno de una arquitectura empresarial de información.";
                this.opcion4.Text = "Es una plataforma de permite ofreceser servicios en la nube";
            else
            {
                Application.Exit();
            }

        }

        private void pregunta2()
        {
            this.pregunta.Text = "Gracias a ________________, se puede trasladar cargas de trabajo entre máquinas virtuales según la carga ";
            this.opcion1.Text = "la virtualizacion";
            this.opcion2.Text = "las maquinas virtuales";
            this.opcion3.Text = "el servidor";
            this.opcion4.Text = "a los recursos TI";
            seleccion++;
        }
        private void pregunta3()
        {
            this.pregunta.Text = "Esa es la idea detrás de la virtualización de servidores, y la idea se remonta a los mainframes de IBM en la década de";
            this.opcion1.Text = "1960";
            this.opcion2.Text = "1970";
            this.opcion3.Text = "1975";
            this.opcion4.Text = "1980";
            seleccion++;
        }
        private void pregunta4()
        {
            this.pregunta.Text = "La virtualización de servidores cambió todo eso y ha sido ampliamente adoptado. De hecho, es difícil encontrar una empresa que no esté trabajando la mayor parte de sus cargas de trabajo en un entorno de ____________________ ";
            this.opcion1.Text = "servidores remotos";
            this.opcion2.Text = "sistema operativo";
            this.opcion3.Text = "Un servicio";
            this.opcion4.Text = "Maquina Virtual";
            seleccion++;
        }
        private void pregunta5()
        {
            this.pregunta.Text = "La virtualización de servidores tomó un _____________________ y lo dividió en dos, lo que permite que múltiples sistemas operativos y múltiples aplicaciones completas aprovechen la potencia informática subyacente. ";
            this.opcion1.Text = "dispositivo analogico";
            this.opcion2.Text = "dispositivo virtual";
            this.opcion3.Text = "dispositivo fisico";
            this.opcion4.Text = "dispositivo remoto";
            seleccion++;
        }
        private void pregunta6()
        {
            this.pregunta.Text = "La virtualización de servidores reduce la necesidad de ______________________________, lo que le permite desconectarse de la actualización de hardware";
            this.opcion1.Text = "gastos en capital en hardware nuevo";
            this.opcion2.Text = "implementar nuevos servicios";
            this.opcion3.Text = "desarrollar software nuevo";
            this.opcion4.Text = "Ninguna de las anteriores";
            seleccion++;
        }
        private void pregunta7()
        {
            this.pregunta.Text = "La virtualización de servidores también ofrece la alta disponibilidad, ___________, escalabilidad, agilidad, _____________ y flexibilidad que requieren las empresas altamente conectadas basadas en la web de hoy en día";
            this.opcion1.Text = "velocidad, rendimiento";
            this.opcion2.Text = "eficaccia, durabilidad";
            this.opcion3.Text = "rendimiento, autenticidad";
            this.opcion4.Text = "veracidad, autonomia";
            seleccion++;
        }
        private void pregunta8()
        {
            this.pregunta.Text = "Con la virtualización estándar basada en hipervisor, el hipervisor o monitor de máquina virtual (VMM) se ubica entre el ___________________ y la capa de hardware subyacente, proporcionando los recursos necesarios para los sistemas operativos invitados.";
            this.opcion1.Text = "sistema operativo red";
            this.opcion2.Text = "sistema operativo host";
            this.opcion3.Text = "sistema operativo";
            this.opcion4.Text = "servidor";
            seleccion++;
        }
        private void pregunta9()
        {
            this.pregunta.Text = "Con la virtualización a nivel ______, en lugar de usar un hipervisor, ejecuta una versión separada del ______ de Linux. Esto hace que sea fácil de ejecutar múltiples máquinas virtuales en un único host, con un controlador de dispositivo utilizado para la comunicación entre el principal núcleo de Linuxy las máquinas virtuales..";
            this.opcion1.Text = "sistema";
            this.opcion2.Text = "hipervisor";
            this.opcion3.Text = "kernel";
            this.opcion4.Text = "servicio";
            seleccion++;
        }
        private void pregunta10()
        {
            this.pregunta.Text = "La virtualización a nivel de sistema es:";
            this.opcion1.Text = "todas las máquinas virtuales deben compartir la misma copia del sistema operativo";
            this.opcion2.Text = "permite que diferentes máquinas virtuales tengan diferentes sistemas operativos.";
            this.opcion3.Text = "puede ejecutar entornos múltiples pero lógicamente distintos en una única instancia del kernel del sistema";
            this.opcion4.Text = "todas las anteriores";
            seleccion++;
        }
        private void Limpiar()
        {
            this.opcion1.Checked = true;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            switch (seleccion)
            {
                case 1:
                    if (this.opcion1.Checked == true)
                    {
                        resultado++;
                    }
                    pregunta2();
                    Limpiar();
                    break;
                    
                case 2:
                    if (this.opcion1.Checked == true)
                    {
                        resultado++;
                    }
                    pregunta3();
                    Limpiar();
                    break;

                case 3:
                    if (this.opcion4.Checked == true)
                    {
                        resultado++;
                    }
                    pregunta4();
                    Limpiar();
                    break;

                case 4:
                    if (this.opcion3.Checked == true)
                    {
                        resultado++;
                    }
                    pregunta5();
                    Limpiar();
                    break;

                case 5:
                    if (this.opcion1.Checked == true)
                    {
                        resultado++;
                    }
                    pregunta6();
                    Limpiar();
                    break;

                case 6:
                    if (this.opcion1.Checked == true)
                    {
                        resultado++;
                    }
                    pregunta7();
                    Limpiar();
                    break;

                case 7:
                    if (this.opcion2.Checked == true)
                    {
                        resultado++;
                    }
                    pregunta8();
                    Limpiar();
                    break;

                case 8:
                    if (this.opcion3.Checked == true)
                    {
                        resultado++;
                    }
                    pregunta9();
                    Limpiar();
                    break;

                case 9:
                    if (this.opcion1.Checked == true)
                    {
                        resultado++;
                    }
                    pregunta10();
                    Limpiar();
                    break;

                case 10:
                    if (this.opcion3.Checked == true)
                    {
                        resultado++;
                    }
                    if (resultado >= 7)
                    {
                        MessageBox.Show(resultado.ToString(), "FELICITACIONES", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Application.Exit();
                    }
                    else
                    {
                        MessageBox.Show(resultado.ToString(), "DEBES DE ESTUDIAR UN POCO MAS", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Application.Exit();
                    }

                    break;
                default:
                    break;
            }
        }

        private void opcion1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
